package com.example.mvp.presenter;

import com.example.mvp.CAApplication;
import com.example.mvp.utils.CommonUtils;
import com.example.mvp.view.event.OnCallBackToView;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.mockwebserver.MockWebServer;

@Config(sdk = 27, manifest = Config.NONE, application = CAApplication.class)
@RunWith(RobolectricTestRunner.class)
public abstract class BaseTest implements OnCallBackToView {
    protected CAApplication mApp;
    protected MockWebServer mockWebServer;

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);
        mApp = (CAApplication) RuntimeEnvironment.application.getApplicationContext();
        mockWebServer = new MockWebServer();
        mockWebServer.start();
        CommonUtils.BASE_URL = mockWebServer.url("/").toString();
    }

    @After
    public  void tealDown() throws IOException{
        mockWebServer.close();
    }

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {
    }

    public String getText(String fileName) {
        try {
            InputStream in = BaseTest.class.getResourceAsStream("/unitest/files"+fileName);
            byte buff[] = new byte[1024];
            int len;
            StringBuilder text = new StringBuilder();
            while ((len = in.read(buff))>0){
                text.append(new String(buff,0,len));
            }
            in.close();
            return text.toString();
        }catch (IOException e){
            e.printStackTrace();
            return "";
        }

    }

}
