package com.example.mvp.presenter;

import com.example.mvp.model.entities.EmployeeEntity;
import com.example.mvp.view.event.OnEmployeeCallback;

import org.junit.After;
import org.junit.Assert;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricTestRunner;

import java.io.IOException;
import java.util.List;

import okhttp3.mockwebserver.MockResponse;

@RunWith(RobolectricTestRunner.class)
public class EmployeePresenterTest extends BaseTest implements OnEmployeeCallback {
    private EmployeePresenter mPresenter;

    @Override
    public void setUp() throws IOException {
        super.setUp();
        mPresenter = Mockito.spy(new EmployeePresenter(this));
    }

    public void getAllEmployee(){
        mockWebServer.enqueue(new MockResponse().setBody(getText("Get_all_employee.txt")));
    }

    @After
    public void tealDown() throws IOException{
        mPresenter = null;
    }


    @Override
    public void updateData(List<EmployeeEntity> listData) {
        Assert.assertTrue(true);
    }
}
