package com.example.mvp.presenter;

import com.example.mvp.view.event.OnHomeCallBack;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;


import java.io.IOException;

public class HomePresenterTest extends BaseTest implements OnHomeCallBack {
    private HomePresenter mPresenter;

    @Before
    public void setup() throws IOException {
        super.setUp();
        mPresenter = Mockito.spy(new HomePresenter(this));
    }

    @Test
    public void testCalc() {
        int a = 5;
        int b = 5;
        int c = 0;

        Double kq1 = mPresenter.calcSum(a, b);
        Double kq2 = mPresenter.calcSub(a, b);
        Double kq3 = null;
        try {
            kq3 = mPresenter.calcDiv(a, b);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertEquals(10, (double) kq1, 0.0);
        Assert.assertEquals(0, (double) kq2, 0.0);
        Assert.assertEquals(1, (double) kq3, 0.0);

        try {
            kq3 = mPresenter.calcDiv(a, c);
        } catch (Exception e) {
            Assert.assertTrue(true);
        }
    }

    @After
    public void tealDown() throws IOException{
        mPresenter = null;
    }



}
