package com.example.mvp.presenter;

import com.example.mvp.manager.BaseAPIService;
import com.example.mvp.view.event.OnCallBackToView;

public class HomePresenter extends BasePresenter<OnCallBackToView, BaseAPIService>{
    public HomePresenter(OnCallBackToView mCallBack) {
        super(mCallBack, BaseAPIService.class);
    }

    public double calcSum(double a, double b){
        return a+b;
    }

    public double calcSub(double a, double b){
        return a-b;
    }

    public double calcDiv(double a, double b) throws Exception{
        if (b == 0) throw new ArithmeticException("err: div zero");
        return a/b;
    }
}
