package com.example.mvp.presenter;

import android.util.Log;

import com.example.mvp.CAApplication;
import com.example.mvp.CARequest;
import com.example.mvp.manager.EmployeeMangerApi;
import com.example.mvp.model.entities.EmployeeEntity;
import com.example.mvp.view.event.OnEmployeeCallback;

import java.util.List;


public class EmployeePresenter extends BasePresenter<OnEmployeeCallback, EmployeeMangerApi> {
    private static final String TAG = EmployeePresenter.class.getName();
    private static final String KEY_GET_ALL_EMPLOYEE = "KEY_GET_ALL_EMPLOYEE";

    public EmployeePresenter(OnEmployeeCallback mCallBack) {
        super(mCallBack, EmployeeMangerApi.class);
    }

    public void getAllEmployee() {
//        EmployeeMangerApi api = getWS().create(EmployeeMangerApi.class);
//        api.getAllEmployee().enqueue(new Callback<List<EmployeeEntity>>() {
//            @Override
//            public void onResponse(Call<List<EmployeeEntity>> call, Response<List<EmployeeEntity>> response) {
//                Log.i(TAG, "onresponse...");
//                Log.i(TAG, "Code..." + response.code());
//                if (response.code() == 200){
//                    Log.i(TAG, response.body().toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<EmployeeEntity>> call, Throwable t) {
//                Log.e(TAG, "Err..."+t.getMessage());
//            }
//        });
        CARequest caRequest = new CARequest(System.currentTimeMillis() + "", TAG, KEY_GET_ALL_EMPLOYEE);
        CAApplication.getInstance().callRequest(mApi.getAllEmployee(), caRequest, this);

    }

    @Override
    protected void handleSuccess(Object body, String key) {
        super.handleSuccess(body, key);
        switch (key) {
            case KEY_GET_ALL_EMPLOYEE:
                prepareAPIAllEmployee(body);
                break;
            default:
                break;

        }

    }

    private void prepareAPIAllEmployee(Object body) {
        List<EmployeeEntity> listData = (List<EmployeeEntity>) body;
        Log.i(TAG, listData.toString());

        mCallBack.updateData(listData);
    }
}
