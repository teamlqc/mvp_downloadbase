package com.example.mvp.presenter;

import android.util.Log;

import com.example.mvp.CAApplication;
import com.example.mvp.R;
import com.example.mvp.manager.BaseAPIService;
import com.example.mvp.view.event.OnCallBackToView;

import retrofit2.Response;
import retrofit2.Retrofit;

public abstract class BasePresenter<T extends OnCallBackToView,
        G> implements CAApplication.onExcuteCallBack {
    private static final String TAG = BasePresenter.class.getName();
    private static final int CASE_OK = 200;
    private static final int CASE_404 = 404;
    private static final int CASE_403 = 403;
    private static final int CASE_401 = 401;
    private static final int CASE_400 = 400;
    private static final int CASE_500 = 500;
    protected T mCallBack;
    protected G mApi;
    private Retrofit mRetrofit;

    public BasePresenter(T mCallBack, Class<G> api) {
        this.mCallBack = mCallBack;
        mApi = CAApplication.getInstance().getBaseWS().create(api);
    }

    @Override
    public void excuteSuccess(Response response, String key, String tag) {
        Log.i(TAG, "excuteSuccess.." + key);
        switch (response.code()) {
            case CASE_OK:
                handleSuccess(response.body(), key);
                break;
            case CASE_404:
                handleFailed(R.string.err_404, key);
                break;
            case CASE_403:
                handleFailed(R.string.err_403, key);
                break;
            case CASE_401:
                handleFailed(R.string.err_401, key);
                break;
            case CASE_400:
                handleFailed(R.string.err_400, key);
                break;
            case CASE_500:
                handleFailed(R.string.err_500, key);
                break;
            default:
                break;
        }
    }

    private void handleFailed(int sms, String key) {
        doFailed(CAApplication.getInstance().getString(sms), key, null);
    }

    protected void handleSuccess(Object body, String key) {
        //do nothing
    }

    @Override
    public void doFailed(String errMsg, String key, String tag) {

        Log.i(TAG, "doFailed.." + key);
    }

    @Override
    public void showLoading(String key, String tag) {
        mCallBack.showLoading();
        Log.i(TAG, "showLoading.." + key);
    }

    @Override
    public void hideLoading(String key, String tag) {
        mCallBack.hideLoading();
        Log.i(TAG, "hideLoading.." + key);
    }
}
