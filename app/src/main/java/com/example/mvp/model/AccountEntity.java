package com.example.mvp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AccountEntity implements Serializable {
    @SerializedName("user_name")
    private String userName;

    @SerializedName("password")
    private String password;

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
}
