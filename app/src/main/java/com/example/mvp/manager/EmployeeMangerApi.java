package com.example.mvp.manager;

import com.example.mvp.model.AccountEntity;
import com.example.mvp.model.entities.EmployeeEntity;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface EmployeeMangerApi{

    @GET("employees")
    @Headers("Authorization =  {token}")
    Call<List<EmployeeEntity>> getAllEmployee();


    /**
     *
     * @param id
     * @return
     */
    @GET("employee/{id}")
    Call<EmployeeEntity> getEmployee(@Path("id") String id);

    @POST("login")
    Call<EmployeeEntity> login(@Body() AccountEntity account);
}
