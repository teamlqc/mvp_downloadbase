package com.example.mvp;

import com.example.mvp.model.EmployeeEntity;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface EmployeeManagerAPI {
    @GET("employees")
    Call<List<EmployeeEntity>> getAllEmployee();

    @GET("employees/{id}")
    Call<EmployeeEntity> getEmployee(@Path("id") String id);



}
