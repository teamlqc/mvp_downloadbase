package com.example.mvp.service;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.example.mvp.CAApplication;


import java.util.ArrayDeque;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Response;

public class WSAdapter implements CAApplication.onExcuteCallBack {
    private static final int KEY_SUCCESS = 101;
    private static final int KEY_FAILED = 102;
    private static final int KEY_SHOW_LOADING = 103;
    private static final int KEY_HIDE_LOADING = 104;
    private static final String TAG = WSAdapter.class.getName();
    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            doMessage(msg);
            return false;
        }
    });
    private HashMap<String, CAApplication.onExcuteCallBack> mListener = new HashMap<>();
    private HashMap<String, Call> mListData = new HashMap<>();

    private void doMessage(Message msg) {
        Log.i(TAG, "doMessage ..." + msg.what);
        Object[] data = (Object[]) msg.obj;
        String key = (String) data[0];
        String tag = (String) data[1];
        CAApplication.onExcuteCallBack callBack = mListener.get(tag);
        if (callBack == null) return;
        switch (msg.what) {
            case KEY_SUCCESS:
                Response response = (Response) data[2];
                callBack.excuteSuccess(response, key, tag);
                break;
            case KEY_FAILED:
                String err = (String) data[2];
                callBack.doFailed(err, key, tag);
                break;
            case KEY_SHOW_LOADING:
                callBack.showLoading(key, tag);
                break;
            case KEY_HIDE_LOADING:
                callBack.hideLoading(key, tag);
                break;
            default:
                break;

        }
    }

    @Override
    public void excuteSuccess(Response response, String key, String tag) {
        Log.i(TAG, "doSuccess ..." + key);
        Message msg = new Message();
        msg.what = KEY_SUCCESS;
        msg.obj = new Object[]{key, tag, response};
        msg.setTarget(handler);
        msg.sendToTarget();
    }

    @Override
    public void doFailed(String errMsg, String key, String tag) {
        Log.i(TAG, "doFailed ..." + key);
        Message msg = new Message();
        msg.what = KEY_FAILED;
        msg.obj = new Object[]{key, tag, errMsg};
        msg.setTarget(handler);
        msg.sendToTarget();
    }

    @Override
    public void showLoading(String key, String tag) {
        Log.i(TAG, "showLoading ..." + key);
        Message msg = new Message();
        msg.what = KEY_SHOW_LOADING;
        msg.obj = new Object[]{key, tag};
        msg.setTarget(handler);
        msg.sendToTarget();
    }

    @Override
    public void hideLoading(String key, String tag) {
        Log.i(TAG, "hideLoading ..." + key);
        Message msg = new Message();
        msg.what = KEY_HIDE_LOADING;
        msg.obj = new Object[]{key, tag};
        msg.setTarget(handler);
        msg.sendToTarget();
    }

    public void put(String tag, CAApplication.onExcuteCallBack callback) {
        mListener.remove(tag);
        mListener.put(tag, callback);
    }

    public void addData(String time, Call item) {
        mListData.remove(item);
        mListData.put(time, item);
    }

    @Override
    public Call getCall(String time) {
        Call call = mListData.get(time);
        mListData.remove(time);
        return call;
    }
}
