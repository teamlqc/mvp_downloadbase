package com.example.mvp.service;

import com.example.mvp.CAApplication;
import com.example.mvp.CARequest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WebServiceUtil {

    private CAApplication.onExcuteCallBack mListener;

    public void doRequest(final CARequest caRequest) {
        Call call = mListener.getCall(caRequest.getTime());
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                mListener.excuteSuccess(response, caRequest.getKey(), caRequest.getTag());
                mListener.hideLoading(caRequest.getKey(), caRequest.getTag());
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                mListener.doFailed(t.getMessage(), caRequest.getKey(), caRequest.getTag());
                mListener.hideLoading(caRequest.getKey(), caRequest.getTag());
            }
        });
    }

    public void setExcuteCallBack(CAApplication.onExcuteCallBack event) {
        mListener = event;
    }
}
