package com.example.mvp.service;

import android.app.IntentService;
import android.content.Intent;

import com.example.mvp.CAApplication;
import com.example.mvp.CARequest;

public class WSService extends IntentService {
    private static final String NAME = "WSService";

    public WSService() {
        this(NAME);
    }

    public WSService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        CARequest caRequest = (CARequest) intent.getSerializableExtra(CAApplication.KEY_CA_REQUEST);
        WebServiceUtil webServiceUtil = new WebServiceUtil();
        webServiceUtil.setExcuteCallBack(CAApplication.getInstance().getWsAdapter());
        webServiceUtil.doRequest(caRequest);
    }

}
