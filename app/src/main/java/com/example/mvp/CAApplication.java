package com.example.mvp;

import android.app.Application;
import android.content.Intent;

import com.example.mvp.model.entities.EmployeeEntity;
import com.example.mvp.presenter.EmployeePresenter;
import com.example.mvp.service.WSAdapter;
import com.example.mvp.service.WSService;
import com.example.mvp.utils.CommonUtils;


import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CAApplication extends Application {

    public static final String KEY_CA_REQUEST = "KEY_CA_REQUEST";

    private Retrofit mRetrofit;

    private WSAdapter wsAdapter;

    private static CAApplication instance;

    public static CAApplication getInstance(){
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        wsAdapter = new WSAdapter();
    }

    public CAApplication() {
        instance = this;
    }

    public Retrofit getBaseWS(){
        if (mRetrofit == null){
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(CommonUtils.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return mRetrofit;
    }

    public void callRequest(Call call, CARequest caRequest, onExcuteCallBack callback) {
        wsAdapter.put(caRequest.getTag(), callback);
        wsAdapter.addData(caRequest.getTime(), call);
        Intent intent = new Intent(this, WSService.class);
        intent.putExtra(KEY_CA_REQUEST, caRequest);
        startService(intent);
    }

//    public void callRequest(Call call, CARequest caRequest, onExcuteCallBack callback){
//        wsAdapter.put(caRequest.getTag(), callback);
//        wsAdapter.addData(caRequest.getTime(), call);
//        Intent intent = new Intent(this, WSService.class);
//        intent.putExtra(KEY_CA_REQUEST, caRequest);
//        startService(intent);
//    }

    public interface onExcuteCallBack{
        void excuteSuccess(Response response, String key, String tag);
        void doFailed(String errMsg, String key, String tag);
        void showLoading(String key, String tag);
        void hideLoading(String key, String tag);

        default Call getCall(String time){
            return null;
        }
    }

    public WSAdapter getWsAdapter() {
        return wsAdapter;
    }
}
