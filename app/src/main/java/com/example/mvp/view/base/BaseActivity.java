package com.example.mvp.view.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.mvp.presenter.BasePresenter;

import java.lang.reflect.Constructor;

public abstract class BaseActivity<T extends BasePresenter> extends AppCompatActivity {

    protected T mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = getPresenter();
        setContentView(getLayoutId());
        initView();
    }

    protected abstract T getPresenter();

    protected abstract void initView();

    protected abstract int getLayoutId();

    public void showFragment(String tag){
        try {
            //injection
            Class<?> clazz = Class.forName(tag);
            Constructor<?> constructor = clazz.getConstructor();
            BaseFragment frg = (BaseFragment) constructor.newInstance();

            getSupportFragmentManager().beginTransaction().replace(getContentId(),frg).commit();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    protected abstract int getContentId();


}
