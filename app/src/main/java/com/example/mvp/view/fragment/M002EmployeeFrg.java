package com.example.mvp.view.fragment;

import com.example.mvp.R;
import com.example.mvp.model.entities.EmployeeEntity;
import com.example.mvp.presenter.EmployeePresenter;
import com.example.mvp.view.base.BaseFragment;
import com.example.mvp.view.event.OnEmployeeCallback;

import java.util.List;

public class M002EmployeeFrg extends BaseFragment<EmployeePresenter> implements OnEmployeeCallback {

    public static final String TAG = M002EmployeeFrg.class.getName();

    @Override
    protected EmployeePresenter getPresenter() {
        return new EmployeePresenter(this);
    }

    @Override
    protected void initView() {
        mPresenter.getAllEmployee();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_download;
    }

    @Override
    public void updateData(List<EmployeeEntity> listData) {

    }
}
