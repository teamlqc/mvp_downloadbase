package com.example.mvp.view.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mvp.presenter.BasePresenter;
import com.example.mvp.view.event.OnCallBackToView;

import java.lang.reflect.Constructor;

public abstract class BaseFragment<T extends BasePresenter> extends Fragment implements OnCallBackToView {

    protected T mPresenter;
    protected Context mContext;
    protected View mRootView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(getLayoutId(),container,false);
        mContext = getActivity();
        mPresenter = getPresenter();
        initView();
        return mRootView;
    }

    protected abstract T getPresenter();

    protected abstract void initView();

    protected abstract int getLayoutId();

    public <G extends View> G findViewById(int idView){
//        G view = mRootView.findViewById(idView);
        return findViewById(idView,null);
    }

    public <G extends View> G findViewById(int idView,View.OnClickListener event){
        G view = mRootView.findViewById(idView);
        if(view instanceof TextView && event!=null){
            view.setOnClickListener(event);
        }
        return view;
    }

    protected void showNotify(String text) {
        Toast.makeText(mContext, ""+text, Toast.LENGTH_SHORT).show();
    }

    protected void showNotify(int text) {
        Toast.makeText(mContext, ""+text, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void showLoading() {
        showNotify( "Start Calling API...");
    }

    @Override
    public void hideLoading() {
        showNotify( "End Calling API done...");
    }
}
