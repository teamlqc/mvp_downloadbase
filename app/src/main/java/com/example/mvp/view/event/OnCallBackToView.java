package com.example.mvp.view.event;

public interface OnCallBackToView {

    void showLoading();

    void hideLoading();
}
