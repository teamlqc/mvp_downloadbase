package com.example.mvp.view.fragment;

import android.view.View;

import com.example.mvp.R;
import com.example.mvp.presenter.M001EmployeePresenter;
import com.example.mvp.view.base.BaseFragment;
import com.example.mvp.view.event.OnEmployeeCallBack;

public class M001EmployeeFrg extends BaseFragment<M001EmployeePresenter> implements OnEmployeeCallBack, View.OnClickListener {

    public static final String TAG = M001EmployeeFrg.class.getName();


    @Override
    protected M001EmployeePresenter getPresenter() {
        return new M001EmployeePresenter(this);
    }

    @Override
    protected void initView() {
        mPresenter.getAllEmployee();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.frg_m001_employee;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            default:
                break;
        }
    }


}
