package com.example.mvp.view.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import com.example.mvp.R;
import com.example.mvp.presenter.HomePresenter;
import com.example.mvp.view.base.BaseActivity;
import com.example.mvp.view.event.OnHomeCallBack;
import com.example.mvp.view.fragment.M002EmployeeFrg;

public class HomeActivity extends BaseActivity<HomePresenter> implements OnHomeCallBack {
    private static final int MY_PERMISSIONS_REQUEST_WRITE = 1223;

    @Override
    protected HomePresenter getPresenter() {
        return new HomePresenter(this);
    }

    @Override
    protected void initView() {
        checkPermissions();
        showFragment(M002EmployeeFrg.TAG);
    }

    private void checkPermissions() {
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
            if (ActivityCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE);
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_home;
    }

    @Override
    protected int getContentId() {
        return R.id.ln_content;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
