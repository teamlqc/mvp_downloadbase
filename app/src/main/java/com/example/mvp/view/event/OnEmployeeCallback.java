package com.example.mvp.view.event;

import com.example.mvp.model.entities.EmployeeEntity;

import java.util.List;

public interface OnEmployeeCallback extends OnCallBackToView{
    void updateData(List<EmployeeEntity> listData);
}
