package com.example.mvp;


import java.io.Serializable;


public class CARequest implements Serializable {
    String key;
    String tag;
    String time;

    public CARequest(String time, String tag, String key) {
        this.key = key;
        this.tag = tag;
        this.time = time;
    }

    public String getKey() {
        return key;
    }

    public String getTag() {
        return tag;
    }

    public String getTime() {
        return time;
    }
}
